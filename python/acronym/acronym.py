import re

def abbreviate(words):
    return ''.join(re.findall(r"([a-zA-Z])[a-zA-Z']*[^a-zA-Z]*", words)).upper()
    