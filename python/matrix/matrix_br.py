class Matrix(object):
    def __init__(self, matrix_string):
        self.matrix_string = matrix_string
        self.rows = []
        self.columns = []
        self.populate()

    def row(self, index):
        return self.rows[index-1]

    def column(self, index):
        return self.columns[index-1]

    def populate(self):
        lines = self.matrix_string.splitlines()
        for line in lines:
            lst= line.split(" ")
            self.rows.append([int (x) for x in lst])
        
        nb_rows,nb_cols=len(self.rows),len(self.rows[0])
        i=0
        while i<nb_cols:
            col=[]
            j=0
            while j<nb_rows:
                col.append(self.rows[j][i])
                j+=1
            self.columns.append(col)
            i+=1
