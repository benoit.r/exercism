import re
from collections import Counter

def word_count(phrase):
    return Counter(re.findall(r"[a-z0-9-]+(?:\'[a-z])?", phrase.lower()))
