import re

def word_count(phrase):
    words=re.findall(r"[a-z0-9-]+(?:\'[a-z])?", phrase.lower())
    dico={}
    nb_mots = len(words)
    for word in words:
        count=idx=0
        while idx < nb_mots:
            if word == words[idx]:
                count+=1
            idx+=1
        dico[word]=count
    return dico

