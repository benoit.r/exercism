def raindrops(number):
    
    lst = [i for i in range(1,number+1) if number % i == 0]
    res = ""
    if 3 in lst:
        res += 'Pling'
    if 5 in lst:
        res += 'Plang'
    if 7 in lst:
        res += 'Plong'
    if res == "":
        res = str(number)
 
    return res