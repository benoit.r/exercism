def is_isogram(string :str) -> bool :
    
    return not [ char for char in string.lower() if char.isalpha() and string.lower().count(char) > 1]