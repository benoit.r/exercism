from collections import Counter

def is_isogram(string :str) -> bool :
    return not [count for char,count in Counter(list(string.lower())).items() if char not in (' ','-') and count > 1]
