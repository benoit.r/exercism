class Garden(object):
    
    def __init__(self, diagram, students=None):
        self.plant_list={'C':'Clover','G':'Grass','R':'Radishes','V':'Violets'}
        self.plant_rows=diagram.splitlines()
        self.students=sorted(students) if students else ['Alice', 'Bob', 'Charlie', 'David','Eve', 'Fred', 'Ginny', 'Harriet', 'Ileana', 'Joseph', 'Kincaid', 'Larry']

    def plants(self, student):
        start = self.students.index(student) * 2
        return [self.plant_list[self.plant_rows[i][j]] for i in range(2) for j in range(start, start+2)]
