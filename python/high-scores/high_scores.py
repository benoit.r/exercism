class HighScores(object):
    def __init__(self, scores):
        self.scores=scores

    def latest(self):
        return self.scores[-1]

    def personal_best(self):
        return max(self.scores)
    
    def personal_top(self):
        return sorted(self.scores, reverse=True)[:3]

    def report(self):
        latest, personal_best = self.latest(), self.personal_best()
        if latest == personal_best:
            return "Your latest score was {}. That's your personal best!".format(latest)
        
        return "Your latest score was {}. That's {} short of your personal best!".format(latest,personal_best-latest)
