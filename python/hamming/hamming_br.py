def distance(strand_a, strand_b):
    if len(strand_a)!=len(strand_b):
        raise ValueError("Strands size mismatch")
    cpt =  idx = 0
    lng =  len(strand_a)
    while idx < lng:
        cpt += strand_a[idx] != strand_b[idx]
        idx+=1
    return cpt